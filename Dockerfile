FROM alpine:3.9.4
WORKDIR /App
COPY samp6.tar /App 
RUN cd /App && \
    tar -xvf samp6.tar
RUN apk add --update build-base automake autoconf && \
    cd samplicator-76bdfaeb30ffae196302a92371e4b1fb9e46363b && \
    ./autogen.sh && \
    ./configure && \
    make && \
    make install
ENTRYPOINT ["/usr/local/bin/samplicate"] 
